﻿using System.Text;

namespace EncryptAndDecrypt.MD5
{
    public static class Md5Encryption
    {
        /// <summary>
        ///     加密
        /// </summary>
        public static bool Encrypt(string plainText, out string outCipherText)
        {
            outCipherText = null;

            System.Security.Cryptography.MD5 md5 = null;

            try
            {
                md5 = System.Security.Cryptography.MD5.Create();
                var plainByteArray = md5.ComputeHash(Encoding.UTF8.GetBytes(plainText));
                var cipherText     = Convert.ToBase64String(plainByteArray);

                outCipherText = cipherText;

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                md5?.Dispose();
            }
        }
    }
}