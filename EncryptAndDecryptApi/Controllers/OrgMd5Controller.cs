using System.Security.Cryptography;
using EncryptAndDecrypt.MD5;
using EncryptAndDecryptApi.Model;
using Microsoft.AspNetCore.Mvc;

namespace EncryptAndDecryptApi.Controllers;

[ApiController]
[Route("[controller]")]
public class OrgMd5Controller : ControllerBase
{
    public OrgMd5Controller()
    {
    }

    [HttpPost(Name = "Md5")]
    public IEnumerable<string> Get(List<OrgInfo> orgs)
    {
        var data = orgs.Select
        (
            x => Md5Encryption.Encrypt(x.Id, out string md5) ? md5 : string.Empty
        );

        return data;
    }
}