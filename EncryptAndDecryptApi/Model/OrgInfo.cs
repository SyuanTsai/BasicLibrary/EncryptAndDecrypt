﻿namespace EncryptAndDecryptApi.Model;

/// <summary>
///     公司資訊
/// </summary>
public class OrgInfo
{
    /// <summary>
    ///     Key
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    ///     代號
    /// </summary>
    public string Code { get; set; }

    /// <summary>
    ///     名稱
    /// </summary>
    public string Name { get; set; }
}